

resource "aws_vpc" "my_vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "terraform_vpc"

  }
}

resource "aws_internet_gateway" "my_igw" {
  vpc_id = aws_vpc.my_vpc.id

  tags = {

    Name = "terraform_igw"
  }

}

resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.my_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my_igw.id
  }

  tags = {
    Name = "terraform_public_route_table"
  }
}

resource "aws_default_route_table" "private_route_table" {
  default_route_table_id = aws_vpc.my_vpc.default_route_table_id


  tags = {
    Name = "terraform_private_route_table"
  }
}

resource "aws_subnet" "public_subnet" {
  cidr_block              = "10.0.1.0/24"
  vpc_id                  = aws_vpc.my_vpc.id
  availability_zone       = "eu-west-2a"
  map_public_ip_on_launch = true

  tags = {

    Name = "terraform_public_subnet"
  }
}

resource "aws_subnet" "private_subnet" {
  cidr_block        = "10.0.2.0/24"
  vpc_id            = aws_vpc.my_vpc.id
  availability_zone = "eu-west-2b"
  tags = {
    Name = "terraform_private_subnet"
  }
}


resource "aws_route_table_association" "public_route_table_association" {
  route_table_id = aws_route_table.public_route_table.id
  subnet_id      = aws_subnet.public_subnet.id
  depends_on     = [aws_route_table.public_route_table, aws_subnet.public_subnet]


}


resource "aws_route_table_association" "private_route_table_association" {
  route_table_id = aws_default_route_table.private_route_table.id
  subnet_id      = aws_subnet.private_subnet.id
  depends_on     = [aws_default_route_table.private_route_table, aws_subnet.private_subnet]

}

resource "aws_security_group" "my_security_group" {
  name   = "my_test_security_group"
  vpc_id = aws_vpc.my_vpc.id
}

resource "aws_security_group_rule" "ssh_inbound_access" {
  from_port         = 22
  protocol          = "tcp"
  security_group_id = aws_security_group.my_security_group.id

  to_port     = 22
  type        = "ingress"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "http_inbound_access" {
  from_port         = 80
  protocol          = "tcp"
  security_group_id = aws_security_group.my_security_group.id
  to_port           = 80
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]

}

resource "aws_security_group_rule" "all_outbound_access" {
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.my_security_group.id
  to_port           = 0
  type              = "egress"
  cidr_blocks       = ["0.0.0.0/0"]
}










